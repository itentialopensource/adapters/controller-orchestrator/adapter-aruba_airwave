# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Aruba_airwave System. The API that was used to build the adapter for Aruba_airwave is usually available in the report directory of this adapter. The adapter utilizes the Aruba_airwave API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Aruba AirWave adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Aruba AirWave. 

With this adapter you have the ability to perform operations with Aruba AirWave such as:

- Create Site Changes
- Search
- Query

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
