
## 0.2.1 [01-09-2023]

* Fixed entity paths and updated package-lock

See merge request itentialopensource/adapters/controller-orchestrator/adapter-aruba_airwave!2

---

## 0.2.0 [11-03-2022]

* Add new call

See merge request itentialopensource/adapters/controller-orchestrator/adapter-aruba_airwave!1

---

## 0.1.1 [10-31-2022]

* Bug fixes and performance improvements

See commit 365fc74

---
