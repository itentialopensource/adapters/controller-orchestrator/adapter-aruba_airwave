# Aruba AirWave

Vendor: Aruba Networks
Homepage: https://www.arubanetworks.com/

Product: AirWave
Product Page: https://www.arubanetworks.com/products/network-management-operations/airwave/

## Introduction
We classify Aruba AirWave into the Data Center domain as Aruba AirWave provides comprehensive network management and monitoring capabilities for maintaining and optimizing network performance. We also classify Aruba AirWave into the Network Services domain as it provides network management services for wireless, wired and remote networks.

"AirWave is a powerful tool and easy-to-use network operations system that manages Aruba wireless, wired, and remote access networks, as well as wired and wireless infrastructures from a wide range of third-party manufacturers." 

## Why Integrate
The Aruba AirWave adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Aruba AirWave. 

With this adapter you have the ability to perform operations with Aruba AirWave such as:

- Create Site Changes
- Search
- Query

## Additional Product Documentation
The [API documents for Aruba AirWave](https://www.arubanetworks.com/techdocs/AirWave/82130/Content/API%20Guide/API%20Guide.htm)