
## 0.4.6 [10-15-2024]

* Changes made at 2024.10.14_19:48PM

See merge request itentialopensource/adapters/adapter-aruba_airwave!16

---

## 0.4.5 [08-22-2024]

* turn off save metric

See merge request itentialopensource/adapters/adapter-aruba_airwave!14

---

## 0.4.4 [08-22-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-aruba_airwave!13

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_17:55PM

See merge request itentialopensource/adapters/adapter-aruba_airwave!12

---

## 0.4.2 [08-06-2024]

* Changes made at 2024.08.06_19:09PM

See merge request itentialopensource/adapters/adapter-aruba_airwave!11

---

## 0.4.1 [08-06-2024]

* Changes made at 2024.08.06_09:59AM

See merge request itentialopensource/adapters/adapter-aruba_airwave!10

---

## 0.4.0 [07-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-aruba_airwave!9

---

## 0.3.4 [03-28-2024]

* Changes made at 2024.03.28_13:34PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-aruba_airwave!8

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_13:46PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-aruba_airwave!7

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:54PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-aruba_airwave!6

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:22AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-aruba_airwave!5

---

## 0.3.0 [12-28-2023]

* Adapter Migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-aruba_airwave!4

---

## 0.2.1 [01-09-2023]

* Fixed entity paths and updated package-lock

See merge request itentialopensource/adapters/controller-orchestrator/adapter-aruba_airwave!2

---

## 0.2.0 [11-03-2022]

* Add new call

See merge request itentialopensource/adapters/controller-orchestrator/adapter-aruba_airwave!1

---

## 0.1.1 [10-31-2022]

* Bug fixes and performance improvements

See commit 365fc74

---
